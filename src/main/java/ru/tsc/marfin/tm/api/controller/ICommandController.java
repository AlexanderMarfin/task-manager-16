package ru.tsc.marfin.tm.api.controller;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void showSystemInfo();

    void showHelp();

    void showCommands();

    void showArguments();

}
